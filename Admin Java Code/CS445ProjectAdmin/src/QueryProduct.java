import java.util.*;
import java.sql.*;

/**
 * This class provides the functionality needed to view all products.
 * @author Yuejin Lai and Rodrigo Veracierto
 * @since 2016-06-30
 */
public class QueryProduct
{
    DBManager dbmanager;

    /**
     * Class constructor
     * @param dbmanager Object on which communications with the database are delegated
     */
    public QueryProduct(DBManager dbmanager)
    {
	this.dbmanager = dbmanager;
    }

    /**
     * Provides a view of all products.
     * @param type Coded value for type of product: (1) Accessory, (2) Camera, and (3) DVR.  
     * @exception SQLException On SQL error.
     */
    public void view(int type) throws SQLException
    {
	String table;

	if (type == 1)
	    {
		table = "Accessory";
	    }
	else if (type == 2)
	    {
		table = "Camera";
	    }
	else
	    {
		table = "DVR";
	    }

	DBTablePrinter.printTable(dbmanager.getConnection(), table);
    }
}