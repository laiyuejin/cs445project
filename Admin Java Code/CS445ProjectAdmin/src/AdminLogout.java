import java.sql.*;

/**
 * This class implements the functionality required for logging out from 
 * the database.
 * @author Yuejin Lai and Rodrigo Veracierto
 * @since 2016-06-30
 */
public class AdminLogout
{
    DBManager dbmanager;

    /**
     * @param dbmanager Object that communicates with the database.
     */
    public AdminLogout(DBManager dbmanager)
    {
	this.dbmanager = dbmanager;
    }

    /**
     * Closes all connections to the database.
     * @exception SQLException On SQL error.
     */
    public void signOut() throws SQLException
    {
	dbmanager.close();
    }
}