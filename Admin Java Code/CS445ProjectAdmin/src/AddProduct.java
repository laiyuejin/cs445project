import java.util.*;
import java.sql.*;

/**
 * This class provides the functionality needed to add
 * a new product to the database.
 * @author Yuejin Lai and Rodrigo Veracierto
 * @since 2016-06-30
 */
public class AddProduct
{
    DBManager dbmanager;

    /**
     * Class constructor
     * @param dbmanager Object on which communications with the database are delegated
     */
    public AddProduct(DBManager dbmanager)
    {
	this.dbmanager = dbmanager;
    }

    /**
     * Adds a new product to the database.
     * <p>
     * The methods prompts the user to enter the name, photo, price and amount of the 
     * new product. Depending on the value of the parameter, the method determines which
     * table needs to be accessed. With all that information, the method then inserts the
     * new product into the corresponding table.
     * @param type Coded value for type of product: (1) Accessory, (2) Camera, and (3) DVR.  
     * @exception SQLException On SQL error.
     * @return True if product was succesfully added and false, otherwise.
     */
    public boolean add(int type) throws SQLException
    {
	Scanner scan = new Scanner(System.in);

	System.out.println("Enter name of product: ");
	String name = scan.nextLine();

	System.out.println("Enter photo: ");
	String photo = scan.next();

	System.out.println("Enter price: ");
	String price = scan.next();

	System.out.println("Enter amount: ");
	String amount = scan.next();

	String table;
	if (type == 1)
	    {
		table = "Accessory";
	    }
	else if (type == 2)
	    {
		table = "Camera";
	    }
	else
	    {
		table = "DVR";
	    }

	String typeId = String.valueOf(type);

	String keyword = table;

	System.out.println("Enter description: ");
	String description = scan.nextLine();
	String weight;
	String sqlUpdate;
	

	if ((type == 1) || (type == 2))
	    {
		System.out.println("Enter weight:");
		weight = scan.next();
		sqlUpdate = "INSERT INTO " + table + " (name, photo, price, amount, typeId, keyword, description, createDate, weight) " + "VALUES (\'" + name + "\',\'" + photo + "\',\'" + price + "\',\'" + amount + "\',\'" + typeId + "\',\'" + keyword + "\',\'" + description + "\'," + "NOW(),\'" + weight + "\');";
	    }
	else
	    {
		sqlUpdate = "INSERT INTO " + table + " (name, photo, price, amount, typeId, keyword, description, createDate) " + "VALUES (\'" + name + "\',\'" + photo + "\',\'" + price + "\',\'" + amount + "\',\'" + typeId + "\',\'" + keyword + "\',\'" + description + "\'," + "NOW());";
	    }

	return dbmanager.executeUpdate(sqlUpdate);
    }
}