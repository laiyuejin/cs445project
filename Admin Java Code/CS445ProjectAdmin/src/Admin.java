import java.sql.*;
import java.util.*;

/**
 * This class provides all the functionality needed
 * by an administrator of the camera store. 
 * <p>
 * The class allows an administrator to login into 
 * the datatbase system, to view the product databases,
 * to add a new product, to delete an existing product,
 * to view orders, to process orders and to exit the system. 
 *
 * @author Yuejin Lai and Rodrigo Veracierto
 * @since 2016-06-30
 */ 
public class Admin
{
    /**
     * The main method provides a menu of options to the administrator,
     * reads the option selected, and executes it. After execution,
     * the menue of options is again shown prompting for a new selection.
     * @param args Unused
     * @exception SQLException On SQL error
     */
    public static void main(String[] args) throws SQLException
    {
	Scanner scan = new Scanner(System.in);
	AdminControl ac = new AdminControl();
	boolean exit = false;
	int selection = 0;

	ac.connect();

	while(!exit)
	    {
		System.out.println("");
		System.out.println("What would you like to do?:");
		System.out.println("1. Login");
		System.out.println("2. View product databases");
		System.out.println("3. Add a product");
		System.out.println("4. Update a product");
		System.out.println("5. Delete a product");
		System.out.println("6. View orders");
		System.out.println("7. Process an order");
		System.out.println("8. Exit");

		selection = scan.nextInt();

		if (selection < 1 || selection > 8)
		    {
			System.out.println("Invalid selection");
		    }
		else if (selection == 1)
                    {
			ac.signUp();
                    }
		else if (selection == 2)
		    {
			ac.view();
		    }
		else if (selection == 3)
                    {
			ac.add();
                    }
		else if (selection == 4)
                    {
			ac.update();
                    }
		else if (selection == 5)
                    {
			ac.delete();
                    }
		else if (selection == 6)
                    {
			ac.viewOrders();
                    }
		else if (selection == 7)
                    {
			ac.process();
                    }
		else if (selection == 8)
                    {
			ac.signOut();
			exit = true;
                    }
	    }
    }
}