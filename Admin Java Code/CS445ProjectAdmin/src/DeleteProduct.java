import java.sql.*;

/**
 * This class provides the functionality needed to delete
 * an existing product from the database.
 * @author Yuejin Lai and Rodrigo Veracierto
 * @since 2016-06-30
 */
public class DeleteProduct
{
    DBManager dbmanager;

    /**
     * Class constructor
     * @param dbmanager Object on which communications with the database are delegated
     */
    public DeleteProduct(DBManager dbmanager)
    {
	this.dbmanager = dbmanager;
    }

    /**
     * Deletes and existing product from the database.
     * @param type Coded value for type of product: (1) Accessory, (2) Camera, and (3) DVR.  
     * @param id Id of product to be deleted
     * @exception SQLException On SQL error.
     * @return True if product was succesfully deleted and false, otherwise.
     */
    public boolean delete(int type, String id) throws SQLException
    {
	String table;

	if (type == 1)
	    {
		table = "Accessory";
	    }
	else if (type == 2)
	    {
		table = "Camera";
	    }
	else
	    {
		table = "DVR";
	    }

	String sqlUpdate = "DELETE FROM " + table + " WHERE (id = " + id + ");";

	return dbmanager.executeUpdate(sqlUpdate);
    }
}