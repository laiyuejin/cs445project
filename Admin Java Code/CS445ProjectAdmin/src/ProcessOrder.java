import java.util.*;
import java.sql.*;

/**
 * This class provides the functionality needed to process an order.
 * @author Yuejin Lai and Rodrigo Veracierto
 * @since 2016-06-30
 */
public class ProcessOrder
{
    DBManager dbmanager;

    /**
     * Class constructor
     * @param dbmanager Object on which communications with the database are delegated
     */
    public ProcessOrder(DBManager dbmanager)
    {
	this.dbmanager = dbmanager;
    }

    /**
     * Processes an order.
     * <p>
     * @param id Order id to be processed.
     * @param ShipCompany Name of company doing the shipment
     * @param Shipno Shipment number
     * @exception SQLException On SQL error.
     * @return True if order was succesfully processed and false, otherwise.
     */
    public boolean process(String id, String ShipCompany, String Shipno) throws SQLException
    {
	String sqlUpdate = "UPDATE Product_Order SET isShip = 1, ShipCompany = \'" + ShipCompany + "\', Shipno = \'" + Shipno + "\' WHERE (id = " + id + ");";

	return dbmanager.executeUpdate(sqlUpdate);
    }
}