import java.sql.*;

/**
 * This class implements the functionality required for logging in into 
 * the database.
 * @author Yuejin Lai and Rodrigo Veracierto
 * @since 2016-06-30
 */
public class AdminLogin
{
    DBManager dbmanager;

    /**
     * @param dbmanager Object that communicates with the database.
     */
    public AdminLogin(DBManager dbmanager)
    {
	this.dbmanager = dbmanager;
    }

    /**
     * Allows to login to the database.
     * @param username Username to login
     * @param password Password to login
     * @return True if login was successful and false, otherwise
     * @exception SQLException On SQL error.
     */
    public boolean signUp(String username, String password) throws SQLException
    {
	boolean result = false;
	String sqlQuery = "Select * FROM Admin WHERE (name = \'"+ username + "\') AND (pwd = \'" + password + "\')";
	ResultSet rs = dbmanager.executeQuery(sqlQuery);

	if (rs.next() == true)
	    {
		result = true;
	    }

	return result;
    }
}