import java.sql.*;
import java.util.*;

/**
 * This class provides all the functionality needed by 
 * the administrator by delegating the corresponding responsabiity
 * to more specialized objects. 
 * @author Yuejin Lai and Rodrigo Veracierto
 * @since 2016-06-30
 */
public class AdminControl
{
    DBManager dbmanager = null;
    boolean login = false;
    AdminLogin adminLogin;
    AddProduct addProduct;
    AdminLogout adminLogout;
    DeleteProduct deleteProduct;
    UpdateProduct updateProduct;
    QueryProduct queryProduct;
    QueryOrders queryOrders;
    ProcessOrder processOrder;

    public AdminControl()
    {
    }

    /**
       Establishes the connection to the database.
     */
    public void connect()
    {
	dbmanager = DBManager.getInstance();
    }

    /**
     * Allows to login to the database.
     * @exception SQLException On SQL error.
     */
    public void signUp() throws SQLException
    {
	if (dbmanager == null)
	    {
		System.out.println("You must connect to the server before logging in.");
	    }
	else
	    {
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter username: ");
		String username = scan.next();
		System.out.println("Enter password: ");
		String password = scan.next();

		adminLogin = new AdminLogin(dbmanager);
		login = adminLogin.signUp(username, password);
		if (login == false)
		    {
			System.out.println("Login failed. Try again.");
		    }
		else
		    {
			System.out.println("You are now logged in as an Administrator");
		    }
	    }
    }

    /**
     * Adds new product to the database.
     * @exception SQLException On SQL error.
     */
    public void add() throws SQLException
    {
	if (login)
	    {
		Scanner scan = new Scanner(System.in);
		int type = 0;
		boolean b;

		while ((type < 1) || (type > 3))
		    {
			System.out.println("What type of product would you like to add?");
			System.out.println("Accessory(1), Camera(2), DVR(3): ");
			type = scan.nextInt();
			if ((type < 1) || (type > 3))
			    {
				System.out.println("Invalid product type.");
			    }
		    }

		addProduct = new AddProduct(dbmanager);
		b = addProduct.add(type);
		if (b)
		    {
			System.out.println("Product successfully added.");
		    }
		else
		    {
			System.out.println("Product add failed.");
		    }
	    }
	else
	    {
		System.out.println("You need to be logged in to add a product.");
	    }
    }

    /**
     * Allows to logout of the database.
     * @exception SQLException On SQL error.
     */
    public void signOut() throws SQLException
    {
	if (login == false)
	    {
		System.out.println("You must be logged in before logging out.");
	    }
	else
	    {
		adminLogout = new AdminLogout(dbmanager);
		adminLogout.signOut();
		login = false;
		System.out.println("You are now logged out. ");
	    }
    }

    /**
     * Deletes a product from the database.
     * @exception SQLException On SQL error.
     */
    public void delete() throws SQLException
    {
	if (login)
	    {
		Scanner scan = new Scanner(System.in);
		int type = 0;
		boolean b;

		while ((type < 1) || (type > 3))
		    {
			System.out.println("What type of product would you like to delete?");
			System.out.println("Accessory(1), Camera(2), DVR(3): ");
			type = scan.nextInt();

			if ((type < 1) || (type > 3))
			    {
				System.out.println("Invalid product type.");
			    }
		    }

		System.out.println("Please enter id of product that you would like to delete:");
		String id = scan.next();

		deleteProduct = new DeleteProduct(dbmanager);
		b = deleteProduct.delete(type, id);
		if (b)
		    {
			System.out.println("Product successfully deleted.");
		    }
		else
		    {
			System.out.println("Product delete failed.");
		    }
	    }
	else
	    {
		System.out.println("You need to be logged to delete a product.");
	    }
    }

    /**
     * Updates a product in the database.
     * @exception SQLException On SQL error.
     */
    public void update() throws SQLException
    {
	if (login)
	    {
		Scanner scan = new Scanner(System.in);
		int type = 0;
		boolean b;

		while ((type < 1) || (type > 3))
		    {
			System.out.println("What type of product would you like to update?");
			System.out.println("Accessory(1), Camera(2), DVR(3): ");
			type = scan.nextInt();

			if ((type < 1) || (type > 3))
			    {
				System.out.println("Invalid product type.");
			    }
		    }

		System.out.println("Please enter id of product that you would like to update:");
		String id = scan.next();

		int fieldType = 0;
		while (fieldType < 1 || fieldType > 2)
		    {
			System.out.println("Which field would you like to update?");
			System.out.println("Price(1), Amount(2)");
			fieldType = scan.nextInt();

			if (fieldType < 1 || fieldType > 2)
			    {
				System.out.println("Invalid field.");
			    }
		    }

		String field;

		if (fieldType == 1)
		    {
			field = "price";
		    }
		else
		    {
			field = "amount";
		    }

		System.out.println("Enter new value: ");
		String fieldValue = scan.next();

		updateProduct = new UpdateProduct(dbmanager);
		b = updateProduct.update(type, id, field, fieldValue);

		if (b)
		    {
			System.out.println("Product successfully updated.");
		    }
		else
		    {
			System.out.println("Product update failed.");
		    }
	    }
	else
	    {
		System.out.println("You need to be logged in to update a product.");
	    }
    }

    /**
     * Allows to view the product database.
     * @exception SQLException On SQL error.
     */
    public void view() throws SQLException
    {
	if (login)
	    {
		Scanner scan = new Scanner(System.in);
		int type = 0;
		boolean b;

		while ((type < 1) || (type > 3))
		    {
			System.out.println("What type of product would you like to view?");
			System.out.println("Accessory(1), Camera(2), DVR(3): ");
			type = scan.nextInt();
			if ((type < 1) || (type > 3))
			    {
				System.out.println("Invalid product type.");
			    }
		    }

		queryProduct = new QueryProduct(dbmanager);
		queryProduct.view(type);
	    }
	else
	    {
		System.out.println("You need to be logged in to view products.");
	    }
    }

    /**
     * Allows to view the orders database.
     * @exception SQLException On SQL error.
     */
    public void viewOrders() throws SQLException
    {
	if (login)
	    {
		Scanner scan = new Scanner(System.in);
		queryOrders = new QueryOrders(dbmanager);
		queryOrders.view();
	    }
	else
	    {
		System.out.println("You need to be logged in to view orders.");
	    }
    }

    /**
     * Allows to process an order.
     * @exception SQLException On SQL error.
     */
    public void process() throws SQLException
    {
	if (login)
	    {
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter id of order that you would like to process:");
		String id = scan.next();
		System.out.println("Enter name of shipment company:");
		String ShipCompany = scan.next();
		System.out.println("Enter shipment number");
		String Shipno = scan.next();

		processOrder = new ProcessOrder(dbmanager);
		boolean b = processOrder.process(id, ShipCompany, Shipno);

		if (b)
		    {
			System.out.println("Order successfully processed.");
		    }
		else
		    {
			System.out.println("Order processing failed.");
		    }
	    }
	else
	    {
		System.out.println("You need to be logged in to process an order.");
	    }
    }
}