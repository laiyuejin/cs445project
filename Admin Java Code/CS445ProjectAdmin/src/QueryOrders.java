import java.util.*;
import java.sql.*;

/**
 * This class provides the functionality needed to view all orders.
 * @author Yuejin Lai and Rodrigo Veracierto
 * @since 2016-06-30
 */
public class QueryOrders
{
    DBManager dbmanager;

    /**
     * Class constructor
     * @param dbmanager Object on which communications with the database are delegated
     */
    public QueryOrders(DBManager dbmanager)
    {
	this.dbmanager = dbmanager;
    }

    /**
     * Provides view of all orders.
     * @exception SQLException On SQL error.
     */
    public void view() throws SQLException
    {
	DBTablePrinter.printTable(dbmanager.getConnection(), "Product_Order");
    }
}