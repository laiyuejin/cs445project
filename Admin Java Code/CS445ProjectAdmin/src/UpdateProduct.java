import java.util.*;
import java.sql.*;

/**
 * This class provides the functionality needed to update a product.
 * @author Yuejin Lai and Rodrigo Veracierto
 * @since 2016-06-30
 */
public class UpdateProduct
{
    DBManager dbmanager;

    /**
     * Class constructor
     * @param dbmanager Object on which communications with the database are delegated
     */
    public UpdateProduct(DBManager dbmanager)
    {
	this.dbmanager = dbmanager;
    }

    /**
     * Updates a product.
     * @param type Coded value for type of product: (1) Accessory, (2) Camera, and (3) DVR.  
     * @param id Id of product to be updated.
     * @param field Product field that will be updated (either "price" or "amount").
     * @param fieldValue New field value.
     * @exception SQLException On SQL error.
     * @return True if product was succesfully updated and false, otherwise.
     */
    public boolean update(int type, String id, String field, String fieldValue) throws SQLException
    {
	String table;

	if (type == 1)
	    {
		table = "Accessory";
	    }
	else if (type == 2)
	    {
		table = "Camera";
	    }
	else
	    {
		table = "DVR";
	    }

	String sqlUpdate = "UPDATE " + table + " SET " + field + " = " + fieldValue + " WHERE (id = " + id + ");";

	return dbmanager.executeUpdate(sqlUpdate);
    }
}