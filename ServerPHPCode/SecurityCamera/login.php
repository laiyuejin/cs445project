<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 6/22/2016
 * Time: 5:32 PM
 */

require_once dirname(__FILE__) . '/DBService.php';
require_once dirname(__FILE__) . '/response.php';
/* 获取post参数 */
$email = $_POST ['email'];
/* MD5加密密码 */
$pwd = $_POST ['password'];

$dbService = DBService::getInstance();
$dbService->connect();
$loginResult = $dbService->login ($email, $pwd );

if(($loginResult == 'success') == FALSE){
    response::show(400,$loginResult,$loginResult);
    return;
}else{//密码账户匹配成功
    response::show(400,"success",$loginResult);//可以设置返回一些用户的个人消息
    return;
}

?>