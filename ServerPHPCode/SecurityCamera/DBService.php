<?php
class DBService {
	private static $_instance;
	private static $_connectSource;

	private $_dbConfig = array (
			'host' => '173.255.213.6',
			'user' => 'root',
			'password' => '123456',
			'database' => 'cs445project'
	);
	private function __construct() {
		
	}
	static public function getInstance() {
		if (! (self::$_instance instanceof self)) {
			self::$_instance = new self ();
		}
		return self::$_instance;
	}
	public function connect() {
		if (! self::$_connectSource) {
			self::$_connectSource = @mysql_connect ( $this->_dbConfig ['host'], $this->_dbConfig ['user'], $this->_dbConfig ['password'] );
			if (! self::$_connectSource) {
				throw new Exception ( 'mysql connect error ' . mysql_error () );
				// 测试使用
				// die('mysql connect error' . mysql_error());
			}
			
			mysql_select_db ( $this->_dbConfig ['database'], self::$_connectSource );
			mysql_query ( "set names UTF8", self::$_connectSource );
		}
		return self::$_connectSource;
	}
	/*
	 * $connect = Db::getInstance()->connect();
	 * $sql = "select * from video";
	 * $result = mysql_query($sql, $connect);
	 * echo mysql_num_rows($result);
	 * var_dump($result);
	 */
	/**
	 * 插入用户提交的建议
	 * 
	 * @return Ambigous <>|string
	 */
	public function insertSuggestion($content, $user_phone) {
		mysql_query ( "INSERT INTO suggestion(content,user_phone) VALUES('$content','$user_phone')", self::$_connectSource );
	}
	/**
	 * 处理用户登录
	 *
	 * @param
	 *        	用户号码$phone
	 * @param 用户密码 $pwd        	
	 * @return 返回登录结果 boolean|NULL
	 */
	public function login($email, $pwd) {
		if ($result = mysql_query ( 'SELECT *  FROM user WHERE email=' . $email, self::$_connectSource )) {
			if ($row = mysql_fetch_array ( $result )) {
				if ($row ["password"] == $pwd) {
					return $row ['name']; // 密码匹配后返回用户的账户名
				} else {
					return "you passwork is wrong";
				}
			}
		} else {
			return "sorry for the fail to connect the database！";
		}
		return "account had been registered";
	}
	
	/**
	 * 处理用户注册
	 *
	 * @param 用户邮箱 $email        	
	 * @param 用户密码 $pwd        	
	 * @return 返回登录结果 boolean|NULL
	 */
	public function register($username, $pwd, $email) {
		$result = mysql_query ( 'SELECT count FROM user WHERE email=' . $email, self::$_connectSource );
		if ($result!=0) {
			return 'phone has been registered';
		} else {
			$result = mysql_query ( "INSERT INTO User (name,pwd,email) VALUES('$username','$pwd','$email')", self::$_connectSource );
			if ($result) {
				return 'success';
			} else {
				return 'database fail';
			}
		}
	}
	
	/**
	 * 执行简单查询
	 *
	 * @param 要执行SQL语句 $sqlstr        	
	 * @return 结果
	 */
	public function query($sqlstr) {
		mysql_query ( "set names 'UTF8'" );
//		var_dump($sqlstr);
		return mysql_query ( $sqlstr, self::$_connectSource );
	}
}