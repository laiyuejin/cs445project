package com.balala.util;


import com.balala.network.imp.ApiException;
import com.balala.network.model.GanWuData;
import com.balala.network.model.GanWuItem;
import com.balala.network.model.News;
import com.socks.library.KLog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import rx.functions.Func1;

/**
 * 用来统一处理Http的resultCode,并将GanWuData的Data部分剥离出来返回给subscriber
 */
public class GanWuDataToItemsMapper implements Func1<GanWuData, List<GanWuItem>> {
    private static GanWuDataToItemsMapper INSTANCE = new GanWuDataToItemsMapper();

    public GanWuDataToItemsMapper() {
    }

    public static GanWuDataToItemsMapper getInstance() {
        return INSTANCE;
    }

    @Override
    public List<GanWuItem> call(GanWuData ganWuData) {
        KLog.a("inCall");
        if (ganWuData.isError()) {
            throw new ApiException(100);
        }
        List<News> ganwus = ganWuData.results.androidList;
        List<News> images = ganWuData.results.妹纸List;
        List<GanWuItem> items = new ArrayList<>(ganwus.size());
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SS'Z'");
        SimpleDateFormat outputFormat = new SimpleDateFormat("yy/MM/dd HH:mm:ss");
        for (News ganwu : ganwus) {
            GanWuItem item = new GanWuItem();
            for (News image : images){
                item.setImageurl(image.getUrl());
            }
            try {
                Date date = inputFormat.parse(ganwu.getCreatedAt());
                item.date = outputFormat.format(date);
            } catch (ParseException e) {
                e.printStackTrace();
                item.date = "unknown date";
            }
            item.setUrl(ganwu.getUrl());
            item.setWho(ganwu.getWho());
            item.setDescription(ganwu.getDesc());
            items.add(item);
        }

        return items;
    }
}