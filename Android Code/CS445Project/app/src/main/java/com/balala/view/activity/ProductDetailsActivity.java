package com.balala.view.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.balala.R;
import com.balala.customeview.CircleProgressView;
import com.balala.database.GlobleData;
import com.balala.database.UserDataPreference;
import com.balala.network.model2.Product;
import com.balala.util.ClipboardUtils;
import com.balala.util.DialogDeal;
import com.balala.util.SnackbarUtil;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * gank.IO详情web页面
 * <p>
 * Tips:做这个界面遇到个问题,设置界面共享元素动画时候
 * Actiivty的跳转,退出界面崩溃,原因是WebView所在父View
 * NestedScrollView没有设置android:transitionGroup="true"属性
 * <p>
 * StackOverFlow的答案:
 * Switching the ScrollView's transitionGroup from false (the default value)
 * to true makes it work because then the ScrollView is being faded in.
 * The ScrollView has a maximum size, while its contents can be enormous.
 */
public class ProductDetailsActivity extends AppCompatActivity {
    private final static String SubmitURL = GlobleData.Server_URL + "/submit_order.php";
    @Bind(R.id.circle_progress)
    CircleProgressView mCircleProgressView;

    @Bind(R.id.progress_bar)
    ProgressBar mProgressBar;

    @Bind(R.id.detail_description)
    TextView descriptionText;

    @Bind(R.id.toolbar)
    Toolbar mToolbar;

    @Bind(R.id.detail_image)
    ImageView mImageView;

    @Bind(R.id.detail_title)
    TextView mTitle;

    @Bind(R.id.detail_source)
    TextView mSource;

    @Bind(R.id.detail_buy_btn)
    FloatingActionButton buyBtn;

    private static final String KEY_URL = "key_url";

    private static final String KEY_TITLE = "key_title";

    private static final String KEY_IMG = "key_img";

    private static final String KEY_USER = "key_user";

    public static final String TRANSIT_PIC = "picture";
    private ProgressDialog progressDialog = null;
    Activity activity;
    public static Product item = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        //设置布局内容
        setContentView(getLayoutId());
        activity = this;
        //初始化黄油刀控件绑定框架
        ButterKnife.bind(this);
        //初始化控件
        initViews(savedInstanceState);
        //初始化ToolBar
        initToolBar();
    }


    @Override
    protected void onDestroy() {

        super.onDestroy();
        ButterKnife.unbind(this);
        //RxBus.getInstance().unregister(this);
    }

    public int getLayoutId() {

        return R.layout.activity_gank_details;
    }

    public void initViews(Bundle savedInstanceState) {

        ViewCompat.setTransitionName(mImageView, TRANSIT_PIC);
        hideProgress();
        Intent intent = getIntent();
        if (intent != null)
            parseIntent(intent);

        initWebSetting();

        mProgressBar.setVisibility(View.GONE);
        descriptionText.setText(item.toString());


        buyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final EditText inputServer = new EditText(activity);
                inputServer.setFocusable(true);

                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setTitle("please input the address").setIcon(
                        R.drawable.ic_fab_add).setView(inputServer).setNegativeButton(
                        "cancel", null);
                builder.setPositiveButton("confirm",
                        new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {
                                String address = inputServer.getText().toString();
                                Toast.makeText(activity, address, Toast.LENGTH_LONG).show();

                                if (item != null) {
                                    progressDialog = ProgressDialog.show(activity, "submit order...",
                                            "waiting....");
                                    AsyncHttpClient client = new AsyncHttpClient();
                                    RequestParams params = new RequestParams(); // 绑定参数
                                    String userid = UserDataPreference.getID(activity);
                                    params.put("userid", userid);
                                    params.put("productName", item.name);
                                    params.put("productId", item.id);
                                    params.put("productPrice", item.price);
                                    params.put("productPhoto", item.photo);
                                    params.put("productType", item.typeId);
                                    params.put("address", address);
                                    client.post(SubmitURL, params,
                                            new AsyncHttpResponseHandler() {

                                                @Override
                                                public void onFailure(int arg0, Header[] arg1,
                                                                      byte[] arg2, Throwable arg3) {
                                                    progressDialog.dismiss();
                                                    DialogDeal.ShowDialog("not net!", activity);
                                                }

                                                @Override
                                                public void onSuccess(int arg0, Header[] arg1,
                                                                      byte[] arg2) {
                                                    try {
                                                        String isoString = new String(arg2,
                                                                "utf-8");
                                                        System.out.println("submit result--" + isoString);
//                                                        JSONObject json = new JSONObject(isoString);
//                                                        String code = json.getString("code");
//                                                        if (code.equals("400")) {
//                                                            String id = json.getJSONArray("data").getJSONObject(0).getString("id");
                                                            DialogDeal.ShowDialog("you order has been confirm",
                                                                    activity);

//                                                        } else if (code.equals("401")) {
//                                                            String result = json.getString("data");
//                                                            System.out.println(result);
//                                                            DialogDeal.ShowDialog(result,
//                                                                    activity);
//                                                        }
                                                        progressDialog.dismiss();
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                            });
                                }


                            }
                        });
                builder.show();
            }
        });


//        mCommonWebView.setWebChromeClient(new CommonWebChromeClient(mProgressBar, mCircleProgressView));
//        mCommonWebView.setWebViewClient(new CommonWebViewClient(GankDetailsActivity.this));
//        mCommonWebView.loadUrl(url);
    }

    public void initToolBar() {

        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(item.name);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_web, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int itemId = item.getItemId();
        switch (itemId) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_share:
                share();
                return true;

            case R.id.action_copy:
                ClipboardUtils.setText(ProductDetailsActivity.this, this.item.name);
                SnackbarUtil.showMessage(descriptionText, "已复制到剪贴板");
                return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onNewIntent(Intent intent) {

        super.onNewIntent(intent);
        parseIntent(intent);
    }

    private void parseIntent(Intent intent) {


        Glide.with(ProductDetailsActivity.this)
                .load(item.photo)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(mImageView);

        mTitle.setText(item.name);
        mSource.setText("$" + item.price);

        if (TextUtils.isEmpty(item.description)) {
            finish();
        }
    }

    private void initWebSetting() {

//        JsHandler jsHandler = new JsHandler(this, mCommonWebView);
//        mCommonWebView.addJavascriptInterface(jsHandler, "JsHandler");
    }

    public static Intent start(Activity activity, Product item) {

        Intent intent = new Intent();
        intent.setClass(activity, ProductDetailsActivity.class);
        ProductDetailsActivity.item = item;
        // activity.startActivity(intent);

        return intent;
    }

    public static boolean start(Activity activity, String url) {

        Intent intent = new Intent();
        intent.setClass(activity, ProductDetailsActivity.class);
        intent.putExtra(KEY_URL, url);
        activity.startActivity(intent);

        return true;
    }


    public void hideProgress() {

        mCircleProgressView.setVisibility(View.GONE);
        mCircleProgressView.stopSpinning();
    }


    private void share() {

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, "Share");
        intent.putExtra(Intent.EXTRA_TEXT, "Share:" + item.name);
        startActivity(Intent.createChooser(intent, item.description));
    }
}
