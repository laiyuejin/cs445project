package com.balala.network.imp;

import android.content.Context;
import android.support.annotation.NonNull;

import com.balala.network.model2.DVRResult;
import com.balala.util.ACache;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subjects.BehaviorSubject;

/**
 * Created by jin on 5/22/2016.
 */
public class GetMeiZhiData {
    private static GetMeiZhiData instance;
    private int dataSource;
    private GetMeiZhiData() {

    }

    public static GetMeiZhiData getInstance() {
        if (instance == null) {
            instance = new GetMeiZhiData();
        }
        return instance;
    }

    public void loadFromNetwork(Context context, int page, BehaviorSubject<DVRResult> behaviorSubject) {
        Subscription subscribe = Network.getRestAPI(Network.BASE_URL).getDVRData(page)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(new Action1<DVRResult>() {
                    @Override
                    public void call(DVRResult data) { //load on network
                        ACache cache = ACache.get(context);
                        String json = new Gson().toJson(data);
                        cache.put("DVR" + page, json, 30);   //save to cache
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<DVRResult>() {
                    @Override
                    public void call(DVRResult data) {
                        behaviorSubject.onNext(data);
                    }
                });
    }

    public Subscription subscribeData(@NonNull Observer<DVRResult> observer, Context context, int page) {
        BehaviorSubject<DVRResult> behaviorSubject = BehaviorSubject.create();
        behaviorSubject.observeOn(AndroidSchedulers.mainThread()).subscribe(observer);
        Subscription subscription = Observable.create(new Observable.OnSubscribe<DVRResult>() {
            @Override
            public void call(Subscriber<? super DVRResult> subscriber) {
                // first load on the Acache
                String json = ACache.get(context).getAsString("DVR" + page);
                if (json == null) { //load one network
                    System.out.println("load on network");
                    loadFromNetwork(context, page, behaviorSubject);
                } else {//load on cache
                    DVRResult data = new Gson().fromJson(json, new TypeToken<DVRResult>() {
                    }.getType());
                    System.out.println("load on cache");
                    subscriber.onNext(data);
                }
            }
        })
                .subscribeOn(Schedulers.io())
                .subscribe(behaviorSubject);
        return subscription;
    }
}
