package com.balala.view.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.balala.R;
import com.balala.network.model2.CameraItem;
import com.balala.util.UiHelper;
import com.balala.view.activity.ProductDetailsActivity;
import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by jin on 5/23/2016.
 */
public class CameraListAdapter extends RecyclerView.Adapter<CameraListAdapter.NewsViewHolder>{
    private List<CameraItem> data;
    private Activity mContext;
    private boolean animateItems = false;
    private int lastAnimatedPosition = -1;
    public CameraListAdapter(List<CameraItem> Data, Activity context) {
        this.data = Data;
        this.mContext = context;
    }

    //自定义ViewHolder类
    class NewsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        @Bind(R.id.news_image)
        ImageView mNewsPhoto;
        @Bind(R.id.news_title)
        TextView mNewsTitle;
        @Bind(R.id.news_card_view)
        CardView card;
        @Bind(R.id.news_abstract)
                TextView mNewsAbstract;
        CameraItem itemData;

        public NewsViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mNewsPhoto.setOnClickListener(this);
            card.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            Intent intent = ProductDetailsActivity.start(mContext, itemData);

//            ActivityOptionsCompat mActivityOptionsCompat;
//            if (Build.VERSION.SDK_INT >= 21)
//            {
//                mActivityOptionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(
//                        getActivity(), holder.getParentView().findViewById(R.id.item_img), GankDetailsActivity.TRANSIT_PIC);
//            } else
//            {
//                mActivityOptionsCompat = ActivityOptionsCompat.makeScaleUpAnimation(
//                        holder.getParentView().findViewById(R.id.item_img), 0, 0,
//                        holder.getParentView().findViewById(R.id.item_img).getWidth(),
//                        holder.getParentView().findViewById(R.id.item_img).getHeight());
//            }

            mContext.startActivity(intent);

        }
    }

    @Override
    public NewsViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.science_news_item, viewGroup, false);
        NewsViewHolder nvh = new NewsViewHolder(v);
        return nvh;
    }

    @Override
    public void onBindViewHolder(NewsViewHolder viewHolder, int i) {
//        runEnterAnimation(viewHolder.itemView, i);
        CameraItem item = data.get(i);
        viewHolder.itemData = item;
            Glide.with(mContext)
                    .load(item.photo)
                    .crossFade() //设置淡入淡出效果，默认300ms，可以传参.crossFade()
                    .into(viewHolder.mNewsPhoto);
        viewHolder.mNewsTitle.setText(item.name);
        viewHolder.mNewsAbstract.setText(item.description);
    }




    @Override
    public int getItemCount() {
        return data.size();
    }

    public void updateItems(List<CameraItem> addData, boolean animated) {
        animateItems = animated;
        lastAnimatedPosition = -1;
        data.addAll(addData);
        notifyDataSetChanged();
    }

    private void runEnterAnimation(View view, int position) {
        if (!animateItems || position >= 2) {
            return;
        }
        if (position > lastAnimatedPosition) {
            lastAnimatedPosition = position;
            view.setTranslationY(UiHelper.getScreenHeight(mContext));
            view.animate()
                    .translationY(0)
                    .setStartDelay(100 * position)
                    .setInterpolator(new DecelerateInterpolator(3.f))
                    .setDuration(700)
                    .start();
        }
    }
}
