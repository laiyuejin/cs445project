package com.balala.network.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by jin on 5/23/2016.
 */
public class NYTNewsItem {
    public String section;
    public String title;
    public String url;
    @SerializedName("thumbnail_standard")
    public String mainImage;

    @SerializedName("abstract")
    public String abstracts;
    @SerializedName("published_date")
    public String date_time;
    @SerializedName("multimedia")
    public List<NYTImage> images;

}
