package com.balala.network.model2;

import java.util.List;

/**
 * Created by jin on 6/23/2016.
 */
public class DVRResult {
    public String code;
    public String message;
    public List<DVRItem> data;
}
