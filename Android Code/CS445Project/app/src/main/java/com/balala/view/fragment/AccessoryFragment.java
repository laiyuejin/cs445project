package com.balala.view.fragment;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import com.balala.R;
import com.balala.customeview.AbsRecyclerViewAdapter;
import com.balala.network.imp.Network;
import com.balala.network.model2.AccessoryItem;
import com.balala.network.model2.AccessoryResult;
import com.balala.view.activity.ProductDetailsActivity;
import com.balala.view.adapter.AccessoryListAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by jin on 6/23/2016.
 */
public class AccessoryFragment extends Fragment {

    @Bind(R.id.recycle)
    RecyclerView mRecyclerView;

    @Bind(R.id.swipe_refresh)
    SwipeRefreshLayout mSwipeRefreshLayout;

    private final static String TIME_FORMAT_1 = "yyyy-MM-dd'T'HH:mm:ss.SS'Z'";

    private final static String TIME_FORMAT_2 = "yy/MM/dd HH:mm:ss";

    private final static String EXTRA_TYPE = "type";

    private int pageNum = 30;

    private int page = 1;

    private static final int PRELOAD_SIZE = 6;

    private List<AccessoryItem> datas = new ArrayList<>();

    private AccessoryListAdapter mAdapter;

    private boolean mIsLoadMore = true;

    private StaggeredGridLayoutManager mLayoutManager;

    private String type;


    public static AccessoryFragment newInstance(String dataType) {
        AccessoryFragment gankFragment = new AccessoryFragment();
        Bundle bundle = new Bundle();
        bundle.putString(EXTRA_TYPE, dataType);
        gankFragment.setArguments(bundle);
        return gankFragment;
    }

    private View rootView;

    @Nullable
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_gank, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        initViews();

    }

    public void initViews() {
        type = getArguments().getString(EXTRA_TYPE);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        mSwipeRefreshLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onGlobalLayout() {
                mSwipeRefreshLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                mSwipeRefreshLayout.setRefreshing(true);
                startGetBeautysByMap();
            }
        });

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new AccessoryListAdapter(mRecyclerView, datas);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addOnScrollListener(OnLoadMoreListener());
    }

    private void startGetBeautysByMap() {
//        GankApi gankApi = RetrofitHelper.getGankApi();

        Network.getRestAPI(Network.BASE_URL).getAccessory().doOnSubscribe(new Action0() {

            @Override
            public void call() {
                mSwipeRefreshLayout.setRefreshing(true);
            }
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<AccessoryResult>() {

                    @Override
                    public void call(AccessoryResult zipItems) {

                        datas.addAll(zipItems.data);
                        finishTask();
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        setRefreshing(false);
                    }
                });
    }

    public void setRefreshing(boolean t) {
        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setRefreshing(t);
        }

    }


    private void finishTask() {
        if (page * pageNum - pageNum - 1 > 0)
            mAdapter.notifyItemRangeChanged(page * pageNum - pageNum - 1, pageNum);
        else
            mAdapter.notifyDataSetChanged();
        if (mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
        mAdapter.setOnItemClickListener(new AbsRecyclerViewAdapter.OnItemClickListener() {

            @Override
            public void onItemClick(int position, AbsRecyclerViewAdapter.ClickableViewHolder holder) {
                AccessoryItem item = datas.get(position);
                if (!type.equals("休息视频")) {
                    //GankDetailsActivity.start(getActivity(), zipItem.url, zipItem.desc,zipItem.imageUrl,zipItem.who);
                    Intent intent = ProductDetailsActivity.start(getActivity(), item);

                    ActivityOptionsCompat mActivityOptionsCompat;
                    if (Build.VERSION.SDK_INT >= 21) {
                        mActivityOptionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(
                                getActivity(), holder.getParentView().findViewById(R.id.item_img), ProductDetailsActivity.TRANSIT_PIC);
                    } else {
                        mActivityOptionsCompat = ActivityOptionsCompat.makeScaleUpAnimation(
                                holder.getParentView().findViewById(R.id.item_img), 0, 0,
                                holder.getParentView().findViewById(R.id.item_img).getWidth(),
                                holder.getParentView().findViewById(R.id.item_img).getHeight());
                    }

                    startActivity(intent, mActivityOptionsCompat.toBundle());

                }
//                else
//                {
//                    VideoWebActivity.launch(getActivity(), zipItem.url);
//                }
            }
        });
    }

    RecyclerView.OnScrollListener OnLoadMoreListener() {
        return new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView rv, int dx, int dy) {
//                boolean isBottom = mLayoutManager.findLastCompletelyVisibleItemPositions(
//                        new int[2])[1] >= mAdapter.getItemCount() - PRELOAD_SIZE;
//                if (!mSwipeRefreshLayout.isRefreshing() && isBottom)
//                {
//                    if (!mIsLoadMore)
//                    {
//                        mSwipeRefreshLayout.setRefreshing(true);
//                        page++;
//                        startGetBeautysByMap();
//                    } else
//                    {
//                        mIsLoadMore = false;
//                    }
//                }
            }
        };
    }
}

