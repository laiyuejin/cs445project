package com.balala.network.model2;

import com.google.gson.annotations.SerializedName;

/**
 * Created by jin on 6/29/2016.
 */
public class ShopItem {
    @SerializedName("product_name")
    public String productName;
    @SerializedName("product_photo")
    public String productPhoto;
    public String ShipCompany;
    public String Shipno;//Ship number
    public String ordertime;
    public String price;
    public String isShop;


    public String toString(){
        return productName+"    $"+price+"   Time:"+ordertime+"\nShipCompany:"+ShipCompany+"\nShipNumber:"+Shipno;
    }
}
