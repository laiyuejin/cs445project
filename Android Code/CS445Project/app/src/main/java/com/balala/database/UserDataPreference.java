package com.balala.database;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;import java.lang.String;

/**
 * 用户的账户信息会存在 文件中而不是数据库中
 * 这个类就是用来负责对该文件进行各项操作
 * @author 阿进
 *
 */
public class UserDataPreference {
	/** 用户信息保存的pre文件名 **/
	public static final String UserPreferencesName = "UserData";
	/** 用户名 **/
	public static final String Name = "name";
	/** 电话 **/
	public static final String Email = "email";
	/** 密码 **/
	public static final String Password = "password";
	/** 用户是否有账号 **/
	public static final String hasAccount = "hasAccount";
	/**id **/
	public static final String ID = "ID";

	/** 删除账户 **/
	public static void deleteUser(Context context) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(
				GlobleData.UserPreferencesName, Context.MODE_PRIVATE);
		Editor editor = sharedPreferences.edit();
		editor.putBoolean(hasAccount, false);
		editor.putString(Name, "");
		editor.putString(Email, "");
		editor.putString(Password, "");
		GlobleData.UsrEmail = "";
		GlobleData.UserPassword = "";
		GlobleData.isLogin = false;
		editor.commit();
	}

	/** 增加账户 **/
	public static void saveUser(Context context, String name, String email,
			String pwd) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(
				GlobleData.UserPreferencesName, Context.MODE_PRIVATE);
		Editor editor = sharedPreferences.edit();
		editor.putBoolean(hasAccount, true);
		editor.putString(Name, name);
		editor.putString(Email, email);
		editor.putString(Password, pwd);
		GlobleData.UserName = name;
		GlobleData.UsrEmail = email;
		GlobleData.UserPassword = pwd;
		GlobleData.isLogin = true;
		editor.commit();
	}

	/** 增加账户 **/
	public static void saveUser(Context context, String name, String email,
								String pwd,String id) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(
				GlobleData.UserPreferencesName, Context.MODE_PRIVATE);
		Editor editor = sharedPreferences.edit();
		editor.putBoolean(hasAccount, true);
		editor.putString(Name, name);
		editor.putString(Email, email);
		editor.putString(Password, pwd);
		editor.putString(ID,id);
		GlobleData.UserName = name;
		GlobleData.UsrEmail = email;
		GlobleData.UserPassword = pwd;
		GlobleData.ID = id;
		GlobleData.isLogin = true;
		editor.commit();
	}


	/** 获取注册的密码 **/
	public static String getPassword(Context c) {
		SharedPreferences sharedPreferences = c.getSharedPreferences(
				GlobleData.UserPreferencesName, Context.MODE_PRIVATE);
		String pwd = sharedPreferences.getString(Password, "");
		return pwd;
	}

	/** 获取id **/
	public static String getID(Context c) {
		SharedPreferences sharedPreferences = c.getSharedPreferences(
				GlobleData.UserPreferencesName, Context.MODE_PRIVATE);
		String pwd = sharedPreferences.getString(ID, "");
		return pwd;
	}

	/**加载用户信息**/
	public static void loading(Context c) {
		SharedPreferences sharedPreferences = c.getSharedPreferences(
				GlobleData.UserPreferencesName, Context.MODE_PRIVATE);
		GlobleData.UserName = sharedPreferences.getString(Name, "");
		GlobleData.UserPassword = sharedPreferences.getString(Password, "");
		GlobleData.UsrEmail = sharedPreferences.getString(Email, "");
		GlobleData.isLogin = true;
//		String pwd = sharedPreferences.getString(Password, "");
	}

	/** 获取用户名 **/
	public static String getName(Context c) {
		SharedPreferences sharedPreferences = c.getSharedPreferences(
				GlobleData.UserPreferencesName, Context.MODE_PRIVATE);
		String name = sharedPreferences.getString(Name, "");
		return name;
	}

	/** 获取电话 **/
	public static String getPhone(Context c) {
		SharedPreferences sharedPreferences = c.getSharedPreferences(
				GlobleData.UserPreferencesName, Context.MODE_PRIVATE);
		String phone = sharedPreferences.getString(Email, "");
		return phone;
	}

	/** 获取是否注册 **/
	public static boolean getHasAccount(Context c) {
		SharedPreferences sharedPreferences = c.getSharedPreferences(
				GlobleData.UserPreferencesName, Context.MODE_PRIVATE);
		boolean has = sharedPreferences.getBoolean(hasAccount, false);
		return has;
	}
}
