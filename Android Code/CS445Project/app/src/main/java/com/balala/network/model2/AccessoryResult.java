package com.balala.network.model2;

import java.util.List;

/**
 * Created by jin on 6/23/2016.
 */
public class AccessoryResult {
    public String code;
    public String message;
    public List<AccessoryItem> data;
}
