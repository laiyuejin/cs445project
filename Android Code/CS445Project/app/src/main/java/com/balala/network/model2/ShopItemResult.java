package com.balala.network.model2;

import java.util.List;

/**
 * Created by jin on 6/29/2016.
 */
public class ShopItemResult {
    public String code;
    public String message;
    public List<ShopItem> data;
}
