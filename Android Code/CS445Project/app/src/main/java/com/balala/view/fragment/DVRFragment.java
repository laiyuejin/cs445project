package com.balala.view.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.balala.R;
import com.balala.func.OnMeizhiTouchListener;
import com.balala.network.imp.GetMeiZhiData;
import com.balala.network.model.Meizhi;
import com.balala.network.model2.DVRItem;
import com.balala.network.model2.DVRResult;
import com.balala.view.activity.GanDailyActivity;
import com.balala.view.activity.ProductDetailsActivity;
import com.balala.view.activity.PictureActivity;
import com.balala.view.adapter.DVRAdapter;
import com.balala.view.base.BaseFragment;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Observer;
import rx.Subscription;

/**
 * Created by jin on 6/23/2016.
 */
public class DVRFragment extends BaseFragment {
    @Bind(R.id.recyclerView)
    RecyclerView mRecyclerView;

    @Bind(R.id.swipe_refresh_layout)
    SwipeRefreshLayout mSwipeRefreshLayout;

    private boolean mIsFirstTimeTouchBottom = true;

    private static final int PRELOAD_SIZE = 10;

    private int mPage = 1;
    /**list data**/
    private List<DVRItem> dvrDatas;

    private boolean mMeizhiBeTouched;

    private View view;

    private LinearLayoutManager linearLayoutManager;

    protected Subscription subscription;

    private DVRAdapter mGanWuAdapter;

    Context context;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = this.getActivity();
        dvrDatas = new ArrayList<>();
        mGanWuAdapter = new DVRAdapter(dvrDatas, context);
        setRetainInstance(true);
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_recyclerview, container, false);
        }
        ButterKnife.bind(this, view);
        trySetupSwipeRefresh();
        initRecyclerView();
        mPage = 1;
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        loadData(false);
    }

    private void initRecyclerView() {
        linearLayoutManager = new LinearLayoutManager(context);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mGanWuAdapter);
        mRecyclerView.addOnScrollListener(getOnBottomListener(linearLayoutManager));
        mGanWuAdapter.setOnMeizhiTouchListener(getOnMeizhiTouchListener());
    }

    private void loadData(boolean clean) {
        mSwipeRefreshLayout.setRefreshing(true);
        subscription = GetMeiZhiData.getInstance().subscribeData(new Observer<DVRResult>() {
            @Override
            public void onCompleted() {
                setRefreshing(false);
            }

            @Override
            public void onError(Throwable e) {
                loadError(e);
            }

            @Override
            public void onNext(DVRResult meizhis) {
                if (clean) dvrDatas.clear();
                mGanWuAdapter.updateItems(meizhis.data, true);

                setRefreshing(false);
            }
        }, getActivity(), mPage);

        this.addSubscription(subscription);
    }


    private void loadError(Throwable throwable) {
        throwable.printStackTrace();
        System.out.println(throwable.toString());
        setRefreshing(false);
        Snackbar.make(mRecyclerView, R.string.snap_load_fail,
                Snackbar.LENGTH_LONG).setAction(R.string.retry, v -> {
        }).show();
    }

    void setRefreshing(boolean b) {
        if (mSwipeRefreshLayout != null)
            mSwipeRefreshLayout.setRefreshing(b);
    }

    void trySetupSwipeRefresh() {
        mSwipeRefreshLayout.setColorSchemeResources(R.color.refresh_progress_3,
                R.color.refresh_progress_2, R.color.refresh_progress_1);
        setRefreshing(true);
        mSwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        mPage = 1;
                        loadData(true);
                    }
                });
    }

    private OnMeizhiTouchListener getOnMeizhiTouchListener() {
        return (v, meizhiView, card, meizhi) -> {
            if (meizhi == null) return;

            Intent intent = ProductDetailsActivity.start(getActivity(), meizhi);
            startActivity(intent);
            if (v == meizhiView && !mMeizhiBeTouched) {
                mMeizhiBeTouched = true;

                mMeizhiBeTouched = false;
//                startPictureActivity(meizhi, meizhiView);




            } else if (v == card) {
//                startGanDailyActivity(meizhi.getDate());






            }
        };
    }

    private void startGanDailyActivity(Date publishedAt) {
        Intent intent = new Intent(context, GanDailyActivity.class);
        intent.putExtra(GanDailyActivity.EXTRA_GAN_DATE, publishedAt);
        startActivity(intent);
    }


    private void startPictureActivity(Meizhi meizhi, View transitView) {
        Intent intent = PictureActivity.newIntent(context, meizhi.getUrl(),
                meizhi.getDesc());
        ActivityOptionsCompat optionsCompat
                = ActivityOptionsCompat.makeSceneTransitionAnimation(
                (Activity) context, transitView, PictureActivity.TRANSIT_PIC);
        try {
            ActivityCompat.startActivity((Activity) context, intent,
                    optionsCompat.toBundle());
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            startActivity(intent);
        }
    }

    RecyclerView.OnScrollListener getOnBottomListener(LinearLayoutManager layoutManager) {
        return new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView rv, int dx, int dy) {
//                boolean isBottom =
//                        layoutManager.findLastCompletelyVisibleItemPosition() >=
//                                mGanWuAdapter.getItemCount() - PRELOAD_SIZE;
//                if (!mSwipeRefreshLayout.isRefreshing() && isBottom) {
//                    if (!mIsFirstTimeTouchBottom) {
//                        mPage += 1;
//                        loadData(false);
//                    } else {
//                        mIsFirstTimeTouchBottom = false;
//                    }
//                }
            }
        };
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

}
