package com.balala.network.imp;

import android.content.Context;
import android.support.annotation.NonNull;

import com.balala.network.model2.CameraResult;
import com.balala.util.ACache;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subjects.BehaviorSubject;

/**
 * Created by jin on 5/23/2016.
 */
public class GetNYTNewsData {
    private static GetNYTNewsData instance;

    private int dataSource;


    private GetNYTNewsData() {

    }

    public static GetNYTNewsData getInstance() {
        if (instance == null) {
            instance = new GetNYTNewsData();
        }
        return instance;
    }


    public void loadFromNetwork(Context context, int page, BehaviorSubject<CameraResult> behaviorSubject) {
        Network.getRestAPI(Network.BASE_URL).getCamera()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
               .doOnNext(new Action1<CameraResult>() {
                   @Override
                   public void call(CameraResult data) { //load on network

                       ACache cache = ACache.get(context);
                       String json = new Gson().toJson(data);
                       cache.put("camera" + page, json, 10);   //save to cache
                   }
               })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<CameraResult>() {
            @Override
            public void call(CameraResult data) {
                behaviorSubject.onNext(data);
                behaviorSubject.onCompleted();
            }
        });
    }



    public Subscription subscribeData(@NonNull Observer<CameraResult> observer, Context context, int page) {
        BehaviorSubject<CameraResult> behaviorSubject = BehaviorSubject.create();
        behaviorSubject.observeOn(AndroidSchedulers.mainThread()).subscribe(observer);
        Subscription subscription = Observable.create(new Observable.OnSubscribe<CameraResult>() {
            @Override
            public void call(Subscriber<? super CameraResult> subscriber) {
                // first load on the Acache
                String json = ACache.get(context).getAsString("sciences_news" + page);
                if (json == null) { //load one network
                    System.out.println("load on network");
                    loadFromNetwork(context, page, behaviorSubject);
                } else {//load on cache
                    CameraResult data = new Gson().fromJson(json, new TypeToken<CameraResult>() {
                    }.getType());
                    System.out.println("load on cache");
                    subscriber.onNext(data);
                    subscriber.onCompleted();
                }
            }
        })
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(behaviorSubject);
        return subscription;
    }
}