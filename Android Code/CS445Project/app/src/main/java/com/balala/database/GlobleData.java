package com.balala.database;

import java.lang.Double;import java.lang.String;public class GlobleData {
	/**数据库名**/
	public static String DB_NAME = "bangtai.db";
	/**长连接的服务器地址**/
	public static String SocketConnection = "192.168.94.1";
	/**长连接的端口号**/
	public static int SocketPort = 5222;
	/** 服务器名称 **/
	public static final String SocketEndName = "@jin";
//	/** SD卡的路径 **/
//	public static String SDcardPaht = SDcardUtil.getSDcardPath();
	/** 日志保存目录 */
//	public static final String LOGPATH = SDcardPaht + "/bangtai/logs";
	/**用户信息保存的pre文件名**/
	public static final String UserPreferencesName = "UserData";
	/**服务器地址**/
	public static final String Server_URL = "http://104.194.124.23/SecurityCamera";
	/** SD卡得最小存储空间 **/
	public static final long MIN_SPACE_SIZE = 5000;

	/** 网络状态判断 */
	public static boolean isNetConnect = true;
	/**用户的电话**/
	public static String UsrEmail="";
	/**用户的姓名**/
	public static String UserName ="";
	/**用户的密码**/
	public static String UserPassword="";
	public static String ID = "";

	/**客户端的系统版本  1 表示为android 系统**/
	public static int client_type =1;
	/**登陆判断**/
	public static boolean isLogin = false;
	
	/**软件是否需要升级**/
	public static boolean needUpdate = false;
	/**当前的经纬度**/
	public static Double user_Latitude = 24.8916759;
	/**当前的经纬度**/
	public static Double user_Longtitude = 118.65571;
	/**用户当前选择的子类**/
	public static int selectSubject = 1;
	/** 有接受到短信需要更新界面的广播 **/
	public static String HaveMessageBroadcast = "jin.haveMessage.inCome";
	/**接受到消息发出去的广播**/
	public static String DEFAULT_ACCOUNT = "123456";
	
}
