package com.balala.view.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import com.balala.R;
import com.balala.database.GlobleData;
import com.balala.database.UserDataPreference;
import com.balala.util.DialogDeal;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * 登陆界面 用户输入手机进行登陆
 * @author 阿进
 *
 */
public class Login extends Activity{
	public static String LoginURL = GlobleData.Server_URL + "/login.php";
	public EditText userPhoneEt;
	public EditText passwordEt;
	public Button login;
	public Button register;
	public String email = "";
	public String pwd = "";
	public Login loginContext;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		loginContext = this;
		initView();
	}

	private void initView() {
		userPhoneEt = (EditText) findViewById(R.id.login_username);
		passwordEt = (EditText) findViewById(R.id.login_password);
		login = (Button) findViewById(R.id.login_button);
		login.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				email = userPhoneEt.getText().toString().trim();
				pwd = passwordEt.getText().toString().trim();
				AsyncHttpClient client = new AsyncHttpClient();
				RequestParams params = new RequestParams(); // 绑定参数
				params.put("email", email);
				params.put("password", pwd);
				// try {
				// params.put("app_version",
				// UpdateManager.getVersionName(context));
				// } catch (Exception e1) {
				// e1.printStackTrace();
				// }
				client.post(LoginURL, params, new AsyncHttpResponseHandler() {

					@Override
					public void onFailure(int arg0, Header[] arg1, byte[] arg2,
							Throwable arg3) {
						DialogDeal.ShowDialog("Login fail", loginContext);
					}

					@Override
					public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
						String isoString;
						try {
							isoString = new String(arg2, "utf-8");
							System.out.println("服务器返回值:" + isoString);


							String result="";
							JSONObject json;
							try {
								json = new JSONObject(isoString);
								String resultCode = json.getString("code");
								System.out.println("登陆接口:" + isoString);
								if (resultCode.equals(400)) {// 如果获取的数据库成功
									String userName = json.getString("data");
									result= userName;
								} else {// 登陆失败了
									String reason = json.getString("message");
									DialogDeal.ShowDialog(reason, loginContext);
									result= "";
								}
							} catch (JSONException e1) {
								e1.printStackTrace();
							}

							if(!result.equals("")){//如果result不是空的 那么就是登陆成功了  返回用户名
								UserDataPreference.saveUser(loginContext, result, email, pwd);
//								Intent i = new Intent(OpenfireService.intentRelation);//启动对话连接
//								loginContext.startService(i);
								Intent intent = new Intent(loginContext,//跳转到主页
										MainActivity.class);
								intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
								loginContext.startActivity(intent);
								loginContext.finish();
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});

			}
		});
		register = (Button) findViewById(R.id.register_button);
		register.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(loginContext, Register.class);
				loginContext.startActivity(intent);
			}
		});
	}
}
