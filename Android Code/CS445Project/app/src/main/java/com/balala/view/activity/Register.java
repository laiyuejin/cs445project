package com.balala.view.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.balala.R;
import com.balala.database.GlobleData;
import com.balala.database.UserDataPreference;
import com.balala.util.DialogDeal;
import com.balala.util.UiHelper;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONObject;


/**
 * 注册页面  用户所填的数据项都已经用英文进行准确的命名
 *
 * @author 阿进
 */
public class Register extends Activity {
    /**
     * 注册的URL地址
     **/
    public static String RegisterURL = GlobleData.Server_URL + "/register.php";
    private Button Regeister;
    private EditText Phone;
    private EditText Password;
    private EditText Confirm_psw;
    private EditText Name;
    private ProgressDialog progressDialog = null;
    String phone;
    String pwd;
    String name;
    String confirmPsw;
    String email;
    Activity context;
    private Handler handler = new Handler() {

        public void handleMessage(Message message) {
            switch (message.what) {
                case 1: {// Push注冊成功
//				// //开启Push服务
//				Intent i = new Intent(context, OpenfireService.class);
//				context.startService(i);
                    Intent intent = new Intent(context, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    context.startActivity(intent);
                    context.finish();
                    break;
                }
                case 2: {

                }
                break;
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.register);
        context = this;
        initView();
        Regeister.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                phone = Phone.getText().toString().trim();
                pwd = Password.getText().toString().trim();
                confirmPsw = Confirm_psw.getText().toString();
                name = Name.getText().toString().trim();
                if (pwd.equals("")) {
                    DialogDeal.ShowDialog("not password", context);
                } else if (phone.equals("")) {
                    DialogDeal.ShowDialog("input correct phone", context);
                } else if (name.equals("")) {
                    DialogDeal.ShowDialog("input name", context);
                } else if (confirmPsw.equals("")) {
                    DialogDeal.ShowDialog("confirm password wrong", context);
                } else if (!pwd.equals(confirmPsw)) {
                    DialogDeal.ShowDialog("confirm password wrong", context);
                } else {
                    System.out.println("开始注册......");

                    /***** 判断完毕开始提交 ********/
                    progressDialog = ProgressDialog.show(Register.this, "register",
                            "waiting....");
                    AsyncHttpClient client = new AsyncHttpClient();
                    RequestParams params = new RequestParams(); // 绑定参数
                    params.put("name", name);
                    params.put("password", pwd);
                    params.put("email", phone);
                    client.post(RegisterURL, params,
                            new AsyncHttpResponseHandler() {

                                @Override
                                public void onFailure(int arg0, Header[] arg1,
                                                      byte[] arg2, Throwable arg3) {
                                    progressDialog.dismiss();
                                    DialogDeal.ShowDialog("not net!", context);
                                }

                                @Override
                                public void onSuccess(int arg0, Header[] arg1,
                                                      byte[] arg2) {
                                    try {
                                        String isoString = new String(arg2,
                                                "utf-8");
                                        System.out.println("result--"+isoString);
                                        JSONObject json = new JSONObject(isoString);
                                        String code = json.getString("code");
                                        if (code.equals("400")) {
                                            String id = json.getJSONArray("data").getJSONObject(0).getString("id");
                                            System.out.println("result  id ==="+id);
                                            System.out.println("register data:"+id);
                                            // 存入用户信息
                                            UserDataPreference.saveUser(context, name, phone, pwd,id);
                                            UiHelper.startToMainActivity(context);

                                        } else if (code.equals("401")) {
                                            String result = json.getString("data");
                                            System.out.println(result);
                                            DialogDeal.ShowDialog(result,
                                                    context);
                                        }
                                        progressDialog.dismiss();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                }
            }
        });
    }

    private void initView() {
        Regeister = (Button) findViewById(R.id.register_bt);
        Password = (EditText) findViewById(R.id.register_password);
        Confirm_psw = (EditText) findViewById(R.id.register_password2);
        Name = (EditText) findViewById(R.id.register_username);
        Phone = (EditText) findViewById(R.id.register_phone);
//        Phone.setInputType(InputType.TYPE_CLASS_PHONE);
    }
}