package com.balala.view.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.balala.R;
import com.balala.customeview.PullToRefreshView;
import com.balala.database.UserDataPreference;
import com.balala.network.imp.Network;
import com.balala.network.model2.ShopItem;
import com.balala.network.model2.ShopItemResult;
import com.balala.view.adapter.ShopListAdapter;
import com.socks.library.KLog;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class ShopListActivity extends AppCompatActivity {
    @Bind(R.id.shop_list_toolbar)
    Toolbar mToolbar;
    @Bind(R.id.shop_list_recyclerview)
     RecyclerView recyclerView;
    @Bind(R.id.shop_list_refresh_layout)
    PullToRefreshView mPullToRefreshView;
    List<ShopItem> data;
    private ShopListAdapter adapter;
    Activity activity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_list);
        ButterKnife.bind(this);
        activity = this;
        initView();


    }
    public void initView(){
        mToolbar.setNavigationOnClickListener(v -> ShopListActivity.this.onBackPressed());
        mPullToRefreshView.setRefreshing(true);
        String id = UserDataPreference.getID(activity);
        KLog.e("user id=="+ id);
        Network.getRestAPI(Network.BASE_URL).getYourOrders(id).doOnSubscribe(new Action0() {

            @Override
            public void call() {
                mPullToRefreshView.setRefreshing(true);
            }
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<ShopItemResult>() {

                    @Override
                    public void call(ShopItemResult data) {
                        adapter = new ShopListAdapter(data.data,activity);
                        System.out.println("data size :" + data.data.size());
                        recyclerView.setHasFixedSize(true);
                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(activity);
                        recyclerView.setLayoutManager(layoutManager);
                        recyclerView.setAdapter(adapter);
                        mPullToRefreshView.setRefreshing(false);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        mPullToRefreshView.setRefreshing(false);
                    }
                });
    }

}
