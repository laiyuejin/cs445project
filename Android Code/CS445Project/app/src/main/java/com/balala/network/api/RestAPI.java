/*
 * Copyright (C) 2015 Drakeet gmail.com>
 *
 * This file is part of Meizhi
 *
 * Meizhi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Meizhi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Meizhi.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.balala.network.api;


import com.balala.network.model.GanWuData;
import com.balala.network.model.RandomData;
import com.balala.network.model2.AccessoryResult;
import com.balala.network.model2.CameraResult;
import com.balala.network.model2.DVRResult;
import com.balala.network.model2.ShopItemResult;

import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Retrofit
 */
public interface RestAPI {

    @GET("get_dvr.php" +"/{page}")
    Observable<DVRResult> getDVRData(
            @Path("page") int page);

    @GET("day/{year}/{month}/{day}")
    Observable<GanWuData> getGanWuData(
            @Path("year") int year,
            @Path("month") int month,
            @Path("day") int day);

    @GET("data/{type}/10/{page}")
    Observable<RandomData> getRandomData(
            @Path("type") String type,
            @Path("page") int page);

    @GET("get_camera.php")
    Observable<CameraResult> getCamera(
//            @Query("api-key") String apiKey,
//            @Query("limit") int number,
//            @Query("offset") int page
    );
    @GET("get_accessory.php")
    Observable<AccessoryResult> getAccessory(
    );

    @GET("get_your_order.php/{User}")
    Observable<ShopItemResult> getYourOrders(
            @Query("User") String User
    );
}
